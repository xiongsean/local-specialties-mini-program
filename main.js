import App from './App'

import uView from "uview-ui";
// vuex
import store from './store';
import mixin from '@/common/mixin'
import {
	filter
} from '@/util/common/filter.js';
import {
	common
} from '@/util/common/common.js'
import {
	utils
} from '@/util/utils.js'
import {
	tool
} from '@/util/common/timeUtils.js'

Vue.use(uView);
Vue.prototype.$store = store
Vue.prototype.$utils = utils
Vue.prototype.$tool = tool
Vue.prototype.$common = common

import xnavbar from '@/components/common/snavbar/snavbar.vue'
Vue.component('xnavbar', xnavbar)

import xbutton from '@/components/common/sbutton/sbutton.vue'
Vue.component('xbutton', xbutton)

import xloadmore from '@/components/common/xloadmore/xloadmore.vue'
Vue.component('xloadmore', xloadmore)

Vue.mixin(mixin, filter);

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	store,
	...App
})

// 引入请求封装
require('./util/request/index')(app)

app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	// 引入请求封装
	require('./util/request/index')(app)
	return {
		store,
		app
	}
}
// #endif