const utils = {
	/**
	 * 将字符串转换为数组
	 * @param {Object} url 路由地址
	 * @param {Object} params 参数
	 * @param {String} type 跳转类型
	 * @author xiyexin
	 */
	href: (url, params = {}, type) => {
		uni.$u.route({
			url: url,
			params: params,
			type: type || 'navigateTo'
		})
	},
	/**
	 * @param {String} URL 地址
	 * @returns 地址参数对象
	 */
	getQueryParams: (URL) => {
		return JSON.parse('{"' + decodeURI(URL.split('?')[1]).replace(/&/g, '","').replace(/=/g, '":"') + '"}');
	},
	/**
	 * 将字符串转换为数组
	 * @param {Object} str 字符串数组
	 * @param {Object} char 分隔符
	 * @returns 数组
	 * @author xiyexin
	 */
	formatSplit: (str, char) => {
		if (str) return str.split(char);
		else return []
	},
	/**
	 * 将单词的首字母改成大写
	 * @param {Object} str 字母字符串
	 * @returns 首字母大写的字符串
	 * @author xiyexin
	 */
	cap: (str) => {
		return str.charAt(0).toLocaleUpperCase() + str.slice(1);
	},
	/**
	 * 在给定的数字范围内生成一个随机数
	 * @param {Number} min 最小数
	 * @param {Number} max 最大数
	 * @returns 随机数
	 * @author xiyexin
	 */
	random: (min, max) => {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	},
	/**
	 * 求平均值
	 * @param {Number} nums 数字集合
	 * @returns 平均值
	 * @author xiyexin
	 */
	getAverage: (...nums) => {
		return nums.reduce((a, b) => a + b) / nums.length
	},
	/**
	 * 颠倒字符串
	 * @param {String} str 颠倒字符串
	 * @returns 颠倒后的字符串
	 * @author xiyexin
	 */
	reverseStr: (str) => {
		return str.split('').reverse().join('');
	},
	/**
	 * 检查空数组
	 * @param {Array} arr 数组
	 * @returns true or flase
	 * @author xiyexin
	 */
	arrayIsnotEmpty: (arr) => {
		//Array.isArray 确认是不是数组
		return Array.isArray(arr) && arr.length > 0;
	},
	/**
	 * 随机排序数组
	 * @param {Array} arr 数组
	 * @returns 排序后的数组
	 * @author xiyexin
	 */
	shuffleArray: (arr) => {
		return arr.sort(() => 0.5 - Math.random());
	},
	/**
	 * 合并数组
	 * @param {Array} a 数组
	 * @param {Array} b 数组
	 * @returns 合并后的数组
	 * @author xiyexin
	 */
	mergeArray: (a, b) => {
		//合并数组第二种方法[..a,..b]
		return a.concat(b);
	},
	/**
	 * 合并数组并剔除重复值
	 * @param {Array} a 数组
	 * @param {Array} b 数组
	 * @returns 合并后的数组
	 * @author xiyexin
	 */
	mergeArrayAndRmoveDuplications: (a, b) => {
		return [...new Set(utils.mergeArray(a, b))];
	},
	/**
	 * json字符串转数组
	 * @param {String} str 字符串
	 * @returns 数组
	 * @author xiyexin
	 */
	jsonStrConvertArray: (str) => {
		return eval("(" + str + ")");
	},
	/**
	 * 数组转json字符串
	 * @param {Array} arr 数组转字符串
	 * @returns json字符串
	 * @author xiyexin
	 */
	arrayConvertJsonStr: (arr) => {
		return JSON.stringify(arr);;
	},
	/**
	 * 删除指定索引数组元数
	 * @param {Array} arr 数组转字符串
	 * @param {Number} index 索引
	 * @returns 删除后的数组
	 * @author xiyexin
	 */
	removeIndexEle(arr, index) {
		return arr.splice(index, 1);
	},
	/**
	 * 递归
	 * @param {Object} params 全部数据类别带有子节点
	 * @returns 树形结构
	 * @author xiyexin
	 * @description 递归全部父节点
	 */
	generateOptions: (params) => {
		var result = []
		for (const param of params) {
			if (param.parentId === 0) { // 判断是否为顶层节点
				var parent = {
					'id': param.id,
					'code': param.id,
					'name': param.deptName
				}
				parent.children = getchilds(param.id, params) // 获取子节点
				result.push(parent)
			}
		}
		return result
	},
	/**
	 * @param {Object} id 父级id
	 * @param {Object} array 父级对象数组
	 * @returns 返回子级对象数组
	 * @author xiyexin
	 * @description 递归出所有子级
	 */
	getchilds: (id, array) => {
		const childs = []
		for (const arr of array) { // 循环获取子节点
			if (arr.parentId === id) {
				childs.push({
					'id': arr.id,
					'code': arr.id,
					'name': arr.deptName
				})
			}
		}
		for (const child of childs) { // 获取子节点的子节点
			const childscopy = getchilds(child.id, array) // 递归获取子节点
			if (childscopy.length > 0) {
				child.children = childscopy
			}
		}
		return childs
	},
	//生成32位随机 GUID 数
	getGuid: () => {
		function S4() {
			return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
		}
		return (S4() + S4() + "" + S4() + "" + S4() + "" + S4() + "" + S4() + S4() + S4());
	},
	// 加
	numberAdd: (arg1, arg2) => {
		let r1, r2, m;
		try {
			r1 = arg1.toString().split('.')[1].length;
		} catch (e) {
			r1 = 0;
		}
		try {
			r2 = arg2.toString().split('.')[1].length;
		} catch (e) {
			r2 = 0;
		}
		m = Math.pow(10, Math.max(r1, r2));
		return (arg1 * m + arg2 * m) / m;
	},

	// 减
	numberSub: (arg1, arg2) => {
		let r1, r2, m, n;
		try {
			r1 = arg1.toString().split('.')[1].length;
		} catch (e) {
			r1 = 0;
		}
		try {
			r2 = arg2.toString().split('.')[1].length;
		} catch (e) {
			r2 = 0;
		}
		m = Math.pow(10, Math.max(r1, r2));
		n = r1 >= r2 ? r1 : r2;
		return ((arg1 * m - arg2 * m) / m).toFixed(2);
	},
	// 乘
	numberMul: (arg1, arg2) => {
		var m = 0,
			s1 = arg1.toString(),
			s2 = arg2.toString();
		try {
			m += s1.split(".")[1].length
		} catch (e) {}
		try {
			m += s2.split(".")[1].length
		} catch (e) {}
		return Number(s1.replace(".", "")) * Number(s2.replace(".", "")) / Math.pow(10, m)
	},

	// 除
	numberDiv: (arg1, arg2) => {
		var t1 = 0,
			t2 = 0,
			r1, r2;
		try {
			t1 = arg1.toString().split(".")[1].length
		} catch (e) {}
		try {
			t2 = arg2.toString().split(".")[1].length
		} catch (e) {}
		with(Math) {
			r1 = Number(arg1.toString().replace(".", ""))
			r2 = Number(arg2.toString().replace(".", ""))
			return (r1 / r2) * pow(10, t2 - t1);
		}
	},
	selectDictLabel: (datas, value) => {
		var actions = [];
		Object.keys(datas).some((key) => {
			if (datas[key].dictValue === "" + value) {
				actions.push(datas[key].dictLabel);
				return true;
			}
		});
		return actions.join("");
	}
}
module.exports = {
	utils,
}