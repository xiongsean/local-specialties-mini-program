/**
 * 请求拦截
 * @param {Object} http
 */
module.exports = (vm) => {
	uni.$u.http.interceptors.request.use((config) => { // 可使用async await 做异步操作
			config.data = config.data || {}
			// 根据custom参数中配置的是否需要token，添加对应的请求头
			if (config?.custom?.auth) {
				// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
				// config.header.Authorization = vm.$store.state.$token
			}
			// 可以在此通过vm引用vuex中的变量，具体值在vm.$store.state中
			// console.log(vm.$store.state);
			return config
		}, (config) => // 可使用async await 做异步操作
		Promise.reject(config))
}