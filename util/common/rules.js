let rules = {}

// 汉字、字母、数字、下划线
rules.text1 = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	if (/\s/.test(value)) {
		callback(new Error('不可以输入空格'))
		return
	}
	let patt = /^[\u4e00-\u9fa5\w]+$/
	if (!patt.test(value)) {
		callback(new Error('只能输入汉字、字母、数字、下划线'))
		return
	}
	callback()
}

// 字母、数字、下划线
rules.text2 = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	if (/\s/.test(value)) {
		callback(new Error('不可以输入空格'))
		return
	}
	let patt = /^\w+$/
	if (!patt.test(value)) {
		callback(new Error('只能输入字母、数字、下划线'))
		return
	}
	callback()
}

// 非空格文本
rules.text3 = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	if (/\s/.test(value)) {
		callback(new Error('不可以输入空格'))
		return
	}
	callback()
}

// 手机号码
rules.phone = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	let patt = /^1[0-9]{10}$/
	if (!patt.test(value)) {
		callback(new Error('格式不正确'))
		return
	}
	callback()
}

// 密码
rules.password = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	let patt = /^\S+$/
	if (!patt.test(value)) {
		callback(new Error('不可以输入空格'))
		return
	}
	callback()
}

// 身份证
rules.ID = (rule, value, callback) => {
	if (!value) {
		callback()
		return
	}
	let patt =
		/^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/
	if (!patt.test(value)) {
		callback(new Error('格式不正确'))
		return
	}
	callback()
}

export default rules
