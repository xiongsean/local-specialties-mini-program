/**
 * 时间工具封装
 */
const tool = {
	/**
	 * 时间加减天数
	 * @param {Object} day 天数
	 * @returns 返回时间戳 
	 * @description 当前时间之前为负数（例如前一天（-1）） 之后为（1）
	 * @author xiyexin
	 */
	getTime: (day) => {
		return new Date().getTime() + day * 24 * 3600 * 1000;
	},
	/**
	 * 获取指定月份天数
	 * @param {Object} year 年份
	 * @param {Object} month 月份
	 * @returns 返回指定年月份天数
	 * @description 获取指定月份天数（例如2022,4 则返回30）
	 * @author xiyexin 
	 */
	getDay: (year, month) => {
		let curretMonth = new Date(year, month, 0);
		return curretMonth.getDate();
	},
	/**
	 * 获取指定时间是周几
	 * @param {Object} times 指定时间
	 * @returns 返回周几
	 * @description 获取指定天数是周几
	 * @author xiyexin
	 */
	getWeek: (times) => {
		let myDate = new Date(times)
		let wk = myDate.getDay()
		let weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
		let week = weeks[wk]
		return week;
	},
	/**
	 * includes(字符串,索引)判断字符串中是否存在指定字符
	 * @param {Object} date 指定时间new Date('2022-08-25')
	 * @returns true/false
	 * @description 确认日期是否是周末 
	 * @author xiyexin
	 */
	isWeekend: (date) => {
		return [0, 6].includes(date.getDay());
	},
	/**
	 * @param {String} date1 结束时间new Date('2022-08-25')
	 * @param {String} date2 开始时间new Date('2022-08-25')
	 * @returns 天数
	 * @description 计算间隔天数 
	 * @author xiyexin
	 */
	daysDiff: (date1, date2) => {
		return Math.ceil(Math.abs(date1 - date2) / 86400000);
	}
}

module.exports = {
	tool,
}
