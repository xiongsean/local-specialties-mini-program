export default {
	data() {
		return {
			share: {
				title: '禾汇初味土特产商城',
				path: '/pages/index/index', // 全局分享的路径，比如 首页
				imageUrl: '', // 全局分享的图片(可本地可网络)
			},
		}
	},
	// 定义全局分享
	// 1.发送给朋友
	onShareAppMessage(res) {
		let lifeData = lifeData = uni.getStorageSync('lifeData');
		return {
			title: this.share.title,
			path: this.share.path + '?userId=' + lifeData.$userInfo.id,
			imageUrl: this.share.imageUrl,
		}
	},
	//2.分享到朋友圈
	onShareTimeline(res) {
		let lifeData = lifeData = uni.getStorageSync('lifeData');
		return {
			title: this.share.title,
			path: this.share.path + '?userId=' + lifeData.$userInfo.id,
			imageUrl: this.share.imageUrl,
		}
	},
}