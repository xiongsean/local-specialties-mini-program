export const opts = {
	//柱状图
	columnOpts: {
		color: ["#FF9EA6"],
		padding: [0, 0, 0, 0],
		enableMarkLine: false,
		dataLabel: false,
		legend: {
			show: false
		},
		xAxis: {
			axisLine: false,
			axisLineColor: '#EEEEEE',
			rotateLabel: false,
			disableGrid: true,
			fontColor: '#93949D',
			gridType: 'dash',
		},
		yAxis: {
			showTitle: false,
			dataLabel: false,
			disableGrid: false,
			gridColor: '#EEEEEE',
			gridType: 'dash',
			data: [{
				min: 0,
				fontColor: '#000',
				axisLine: false,
				disabled: false,
				title: false,
				axisLineColor: '#fff',
				fontSize: 1,
				format: ''
			}]
		},
		extra: {
			tooltip: {
				borderWidth: '100rpx',
				bgColor: '#FF2E3F',
			},
			area: {
				opacity: 0.1,
				addLine: false,
				width: 2,
				gradient: false
			},
			column: {
				type: "group",
				width: 18,
				activeBgColor: "#FF2E3F",
				activeBgOpacity: 0.08,
				seriesGap: 5,
				barBorderRadius: [
					6,
					6,
					6,
					6
				]
			},
			markLine: {
				data: [{
					value: 21,
					showLabel: true
				}]
			}
		}
	}
}