/**
 * @description 根据val(数字或字符串) 生成一个字符串并返回,
 * @param val
 * @param digitNum 规定保留多少位小数
 * @returns {string}
 */
function subStringNum(val, digitNum) {
	var a_type = typeof val;
	if (a_type === 'number') {
		var aStr = val.toString();
		var aArr = aStr.split('.');
	} else if (a_type === 'string') {
		aArr = val.split('.');
	}

	if (aArr.length > 1) {
		val = aArr[0] + '.' + aArr[1].substr(0, digitNum);
	}
	return val
}

export const filter = {
	filters: {
		// 首字母大写过滤器
		capitalize: function(value) {
			if (!value) return '';
			value = value.toString();
			return value.charAt(0).toUpperCase() + value.slice(1);
		},
		//保留一位小数
		xToFixed(value, no = 2) {
			if (!value) return 0;
			return parseFloat(value).toFixed(no);

		},
		//千米
		xKilometers(distance) {
			if (distance) {
				return (distance / 1000).toFixed(2);
			} else {
				return 0;
			}
		},
		//格式化（万单位）
		tenMillion_cn(val) {
			if (val > 10000) {
				let num = (val / 10000).toFixed(2);
				return num + '万';
			} else return val;
		},
		//格式化（万）
		tenMillion(val, digitNum = 2) {
			if (val > 10000) {
				val = val / 10000;
			}
			let forFixed = Math.pow(10, digitNum);
			val = Math.round(val * forFixed) / forFixed;
			return subStringNum(val, digitNum);
		},
	},
	methods: {
		//校验输入金额
		oninput(e) {
			let that = this;
			let maxlen = 0;
			that.$nextTick(() => {
				let val = e.toString();
				val = val.replace(/[^\d.]/g, ""); //清除"数字"和"."以外的字符
				val = val.replace(/\.{2,}/g, "."); //只保留第一个. 清除多余的
				val = val.replace(/^0+\./g, '0.');
				val = val.match(/^0+[1-9]+/) ? val = val.replace(/^0+/g, '') : val
				val = (val.match(/^\d*(\.?\d{0,2})/g)[0]) || ''
		
				if (val.includes(".")) {
					let numDian = val.toString().split(".")[1].length;
					if (numDian === 2) {
						maxlen = val.length;
					}
				} else {
					maxlen = 8;
				}
				return {
					maxlen,
					val
				}
			});
		},
		//比较时间
		compareTime(gettime) {
			var today = new Date() //获取当前时间
			gettime = gettime.replace(/-/g, '/'); //转化成时间戳作比较
			var endTime = new Date(gettime) //自己的时间
			if (today.getTime() > endTime.getTime()) {
				return false //当前时间大于我的时间
			} else {
				return true //当前时间小于我的时间
			}
		},
		//将字符转换成数组
		formatSplit(str, char) {
			if (str) return str.split(char);
			else return []
		},
		yesNo(val) {
			if (val === 1) return '是';
			return '否';
		},
	},
};
