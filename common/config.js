module.exports = {
	//#ifdef MP-WEIXIN
	 baseUrl: 'https://tts.fengxing.fun/dev-api',
	 // baseUrl:'http://localhost:8080/dev-api',
	// #endif
	//#ifdef H5
	baseUrl: '/dpc',
	// #endif
	custom: {
		auth: true //请求携带认证值
	},
	responseType: 'blob',
	header: {
		"Content-type": "application/json", // "application/x-www-form-urlencoded"
		// "Authorization": "",
		"company-id":"338c7c05c245490c8a5a648382afb53e"
	}
}