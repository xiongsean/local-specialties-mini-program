const {
	http
} = uni.$u

//获取openId
export const getOpenId = (params) => http.get('/api/userInfo/getOpenId', {
	params
});
//获取login
export const login = (params, config = {}) => http.post('/api/userInfo/login', params, config)
//获取getPhoneNumber
export const getPhoneNumber = (params) => http.get('/api/userInfo/getPhoneNumber', {
	params
});
//更新用户信息
export const updateUserInfo = (params, config = {}) => http.post('/api/userInfo/updateUserInfo', params, config)

//获取用户信息
export const getUserInfo = (params) => http.get('/api/userInfo/getUserInfo', {
	params
});
//首页信息
export const getIndexList = (params) => http.get('/api/index/getIndexList', {
	params
});

//获取定位信息
export const getCityInfoByLatLng = (params) => http.get('/api/userInfo/getCityInfoByLatLng', {
	params
});





//获取地址
export const getUserAddressInfo = (params) => http.get('/api/userInfo/getUserAddressInfo', {
	params
});
//添加地址
export const addUserAddressInfo = (params, config = {}) => http.post('/api/userInfo/addUserAddressInfo', params, config)
//修改地址
export const editUserAddressInfo = (params, config = {}) => http.post('/api/userInfo/editUserAddressInfo', params,
	config)
//删除地址
export const delUserAddressInfo = (params) => http.get('/api/userInfo/delUserAddressInfo', {
	params
});
//地址详情
export const getUserAddressInfoDetails = (params) => http.get('/api/userInfo/getUserAddressInfoDetails', {
	params
});

//账户列表
export const getUserAccountInfo = (params) => http.get('/api/userInfo/getUserAccountInfo', {
	params
});
// 获取单个账户详情 
export const getUserAccountInfoByAccountId = (params) => http.get('/api/userInfo/getUserAccountInfoByAccountId', {
	params
});
// 获取账户流水 
export const getAccountLogList = (params) => http.get('/api/userInfo/getAccountLogList', {
	params
});


// 获取用户优惠券
export const getUserCouponInfoList = (params) => http.get('/api/userInfo/getUserCouponInfoList', {
	params
});
// 获取优惠券
export const getCouponInfoList = (params) => http.get('/api/userInfo/getCouponInfoList', {
	params
});
// 用户领取优惠券 userGetCouponInfo
export const userGetCouponInfo = (params) => http.get('/api/userInfo/userGetCouponInfo', {
	params
});
// 获取分类 getRubbishType
export const getRubbishType = (params) => http.get('/api/userInfo/getRubbishType', {
	params
});
// 根据分类ID查询 产品列表 getProductInfoList
export const getProductInfoList = (params) => http.get('/api/userInfo/getProductInfoList', {
	params
});
// 查询产品详情 getProductInfoDetails
export const getProductInfoDetails = (params) => http.get('/api/userInfo/getProductInfoDetails', {
	params
});
// 添加产品到购物车 
export const addProductToShoppingCar = (params, config = {}) => http.post('/api/userInfo/addProductToShoppingCar',
	params, config)
// 直接购买商品
export const addProductToShoppingCarXiaDan = (params, config = {}) => http.post(
	'/api/userInfo/addProductToShoppingCarXiaDan',
	params, config)

// 查询购物车列表 
export const getShoppingCarList = (params) => http.get('/api/userInfo/getShoppingCarList', {
	params
});
// 修改购物信息
export const updateProductToShoppingCar = (params, config = {}) => http.post('/api/userInfo/updateProductToShoppingCar',
	params, config)
// 删除购物车信息
export const deleteShoppingCarProduct = (params) => http.get('/api/userInfo/deleteShoppingCarProduct', {
	params
});
// 查询选中购物车中的商品
export const getShoppingCarListByIds = (params) => http.get('/api/userInfo/getShoppingCarListByIds', {
	params
});

// 查询选中的优惠券
export const getUserCouponInfo = (params) => http.get('/api/userInfo/getUserCouponInfo', {
	params
});

// 提交订单 
export const submitOrder = (params, config = {}) => http.post('/api/userInfo/submitOrder',
	params, config);
//  进行支付订单
export const payOrder = (params, config = {}) => http.post('/api/userInfo/payOrder',
	params, config);


// 获取订单备注字典信息
export const getDictList = (params) => http.get('/api/userInfo/getDictList', {
	params
});

// 获取用户订单列表
export const getUserOrderInfoList = (params) => http.get('/api/userInfo/getUserOrderInfoList', {
	params
});
// 获取用户订单详情
export const getOrderDetailsByIds = (params) => http.get('/api/userInfo/getOrderDetailsByIds', {
	params
});
// 取消订单
export const cancelOrderInfo = (params) => http.get('/api/userInfo/cancelOrderInfo', {
	params
});
// 根据ID获取新闻详情
export const getNewsDetails = (params) => http.get('/api/userInfo/getNewsDetails', {
	params
});
// 获取新闻列表
export const getNewsList = (params) => http.get('/api/userInfo/getNewsList', {
	params
});
// 洗护扫码
export const updateOrderDetailsQrCode = (params, config = {}) => http.post(
	'/api/visitUserInfo/updateOrderDetailsQrCode',
	params, config);

// 合作商申请(驿站合伙人)
export const addCompanyInfo = (params, config = {}) => http.post('/api/userInfo/addCompanyInfo',
	params, config);
// 获取附近驿站
export const getEquInfoList = (params) => http.get('/api/userInfo/getEquInfoList', {
	params
});
// 获取订单衣服图片
export const getOrderImageList = (params) => http.get('/api/userInfo/getOrderImageList', {
	params
});
// 添加订单衣服图片
export const uploadClothsImage = (params, config = {}) => http.post('/api/visitUserInfo/uploadClothsImage', params,
	config)
// 删除订单衣服图片
export const deleteOrderImageByIds = (params, config = {}) => http.post('/api/visitUserInfo/deleteOrderImageByIds',
	params,
	config)
// 订单完成 
export const orderSuccess = (params, config = {}) => http.post('/api/visitUserInfo/orderSuccess', params,
	config)
// 获取商户用户订单列表
export const getShopUserOrderInfoList = (params) => http.get('/api/visitUserInfo/getShopUserOrderInfoList', {
	params
});
// 将订单推送给平台
export const shareOrderTo = (params) => http.get('/api/visitUserInfo/shareOrderTo', {
	params
});
// 添加商品信息
export const goodsAdd = (params, config = {}) => http.post('/api/goods/add', params,
	config)
//删除商品信息
export const deleteGoods = (params) => http.get('/api/goods/deleteGoods', {
	params
});
//orderQuJian 洗护订单揽件完成
export const orderQuJian = (params, config = {}) => http.post('/api/visitUserInfo/orderQuJian',
	params,
	config)

// 查询退款信息
export const getRefundOrder = (params) => http.get('/api/userInfo/getRefundOrder', {
	params
});
// 订单上门时间获取
export const getOrderToHomeTime = (params) => http.get('/api/userInfo/getOrderToHomeTime', {
	params
});

// 获取公司信息 

export const getCompanyInfo = (params) => http.get('/api/userInfo/getCompanyInfo', {
	params
});
// 店铺日报
export const getShopDay = (params, config = {}) => http.post('/api/visitUserInfo/getShopDay',
	params,
	config)
// 获取店铺日统计
export const getBillInfoList = (params) => http.get('/api/visitUserInfo/billInfoList', {
	params
});

// 获取店铺日统计详情
export const getBillDetailsList = (params) => http.get('/api/visitUserInfo/billDetailsList', {
	params
});

// 店铺京东下单
export const yizhanJDKY = (params) => http.get('/api/visitUserInfo/yizhanJDKY', {
	params
});
// 获取京东快递单号
export const getJdOrderlist = (params) => http.get('/api/visitUserInfo/getJdOrderlist', {
	params
});

// 商户提现
export const shopTixian = (params, config = {}) => http.post('/api/visitUserInfo/shopTixian',
	params,
	config)

// 用户修改上门时间
export const updateOrderToHomeTime = (params, config = {}) => http.post('/api/userInfo/updateOrderToHomeTime',
	params,
	config)
